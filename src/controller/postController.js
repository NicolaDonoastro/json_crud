import { savePostData, getPostData, sortData } from '../utils/index';

class PostController {
  static getPost = (req, res) => {
    const posts = sortData(getPostData());

    res.render('index', { posts });
  };

  static getPostSearch = (req, res) => {
    const { title } = req.params;

    const existData = getPostData();

    const searchPost = existData.filter((post) => post.title === title);

    if (searchPost.length === 0) {
      return res.status(400).send({ error: true, msg: 'Title post not found' });
    }

    return res.send(searchPost);
  };

  static postCreatePost = (req, res) => {
    const existingPosts = getPostData();
    const postData = req.body;

    if (
      postData.id == null ||
      postData.title == null ||
      postData.author == null
    ) {
      return res.status(401).send({ error: true, msg: 'Post data missing' });
    }
    const findExist = existingPosts.find((post) => post.id === postData.id);

    if (findExist) {
      return res
        .status(409)
        .send({ error: true, msg: 'Post id already exist' });
    }
    existingPosts.push(postData);

    savePostData(existingPosts);
    return res.redirect('/posts');
  };

  static getCreatePost = (req, res) => {
    res.render('create');
  };

  static postUpdatePost = (req, res) => {
    const { id } = req.params;

    const postData = req.body;
    const existPosts = getPostData();
    const findExist = existPosts.find((post) => post.id === id);

    if (!findExist) {
      return res.status(409).send({ error: true, msg: 'Id not exist' });
    }

    const updatePost = existPosts.filter((post) => post.id !== id);

    updatePost.push(postData);
    savePostData(updatePost);
    return res.redirect('/posts');
  };

  static getUpdatePost = (req, res) => {
    const { id } = req.params;

    const existingData = getPostData();
    const post = existingData.find((posts) => posts.id === id);

    console.log(post);
    if (!post) {
      res.status(400).send({ error: true, msg: 'data not found' });
    }

    res.render('edit', { post });
  };

  static deletePost = (req, res) => {
    const { id } = req.params;

    const existingPosts = getPostData();

    const filterUser = existingPosts.filter((post) => post.id !== id);

    if (existingPosts.length === filterUser.length) {
      return res
        .status(409)
        .send({ error: true, msg: 'Id post does not exist' });
    }

    savePostData(filterUser);

    return res.redirect('/posts');
  };
}

export default PostController;
