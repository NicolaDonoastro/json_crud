import express from 'express';
import consola from 'consola';
import engine from 'ejs-locals';

import postRouter from './router/post';

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static('public'));

app.engine('ejs', engine);
app.set('view engine', 'ejs');
app.set('views', 'src/views');

app.use('/posts', postRouter);

app.listen(port, () =>
  consola.success(`server start in http://localhost:${port}`)
);
