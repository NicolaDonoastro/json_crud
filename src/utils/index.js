import fs from 'fs';
import path from 'path';

export const savePostData = (data) => {
  const stringifyData = JSON.stringify(data);
  const filePath = path.resolve(__dirname, '../data/data.json');

  fs.writeFileSync(filePath, stringifyData);
};

export const getPostData = () => {
  const filePath = path.resolve(__dirname, '../data/data.json');
  const jsonData = fs.readFileSync(filePath);

  return JSON.parse(jsonData);
};

export const sortData = (data) => {
  const sortedData = data.sort((a, b) => a.id - b.id);

  return sortedData;
};
