import express from 'express';

import PostController from '../controller/postController';

const router = express.Router();

router.get('/', PostController.getPost);
router.get('/search/:title', PostController.getPostSearch);

router.get('/add', PostController.getCreatePost);
router.post('/add', PostController.postCreatePost);

router.get('/update/:id', PostController.getUpdatePost);
router.post('/update/:id', PostController.postUpdatePost);

router.get('/delete/:id', PostController.deletePost);

export default router;
